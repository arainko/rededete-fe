module.exports = {
  devServer: {
    disableHostCheck: true
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: "@use \"@/styles/_variables.scss\" as *;"
      }
    }
  }
};
