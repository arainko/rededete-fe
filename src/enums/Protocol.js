export const Protocol = {
  deletePost: "deletePost",
  deleteComment: "deleteComment",
  addComment: "addComment",
  join: "join",
  postRoom: postId => `post-${postId}`
};
