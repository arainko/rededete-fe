import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import {subscribe} from "@/api/subreddit";

export default createStore({
  state: {
    email: null,
    password: null,
    user: null,
    subs: null
  },
  plugins: [createPersistedState()],
  mutations: {
    loginUser (state, { email, password, user, subs }) {
      state.email = email;
      state.password = password;
      state.user = user;
      state.subs = subs;
    },
    logout (state) {
      state.email = null;
      state.password = null;
      state.user = null;
      state.subs = null;
    },
    subscribe (state, sub) {
      state.subs.push(sub);
    },
    unsubscribe (state, sub) {
      state.subs = state.subs.filter(s => s !== sub);
    },
    updateCredentials (state, { email, password }) {
      state.email = email;
      state.password = password;
    }
  },
  getters: {
    isLoggedIn: state => {
      return !(state.password === null &&
        state.email === null &&
        state.user === null);
    },
    currentUserId: state => {
      return state.user === null ? null : state.user.id;
    },
    sortedSubs: state => {
      return state.subs.sort();
    },
    isSubbed: state => sub => {
      return state.subs ? state.subs.includes(sub.toLowerCase()) : false;
    }
  },
  actions: {
  },
  modules: {
  }
});
