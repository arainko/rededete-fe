import axios from "axios";
import {API, basicAuth} from "./common.js";
import {getSubredditPosts} from "@/api/subreddit";

export const vote = async (postId, status, email, password) => {
  const body = {status};
  return await axios.put(`${API}/api/post/vote/${postId}`, body, basicAuth(email, password));
};

export const deletePost = async (postId, email, password) => {
  return await axios.delete(`${API}/api/post/${postId}`, basicAuth(email, password));
};

export const createPost = async (post, email, password) => {
  return await axios.post(`${API}/api/post`, post, basicAuth(email, password));
};

export const getPostById = async (postId, maybeEmail, maybePassword) => {
  return await axios.get(`${API}/api/post/${postId}`, basicAuth(maybeEmail, maybePassword));
};

export const getPostsContaining = async (query, maybeEmail, maybePassword) => {
  return await axios.get(`${API}/api/post/search`, {
    ...basicAuth(maybeEmail, maybePassword),
    params: {
      content: query
    }
  });
};

export const paginatePosts = async (subIds, page, email, password, sortOrder) => {
  const resolvedSortOrder = sortOrder || "DESC";
  const pageSize = 10;
  const limit = page * pageSize;
  const offset = limit - pageSize;
  const actualLimit = limit + 1;
  const paginatedPosts = await axios.get(`${API}/api/post`, {
    ...basicAuth(email, password),
    params: {
      offset,
      actualLimit,
      subIds: subIds.join(","),
      orderBy: resolvedSortOrder
    }
  });
  return [
    paginatedPosts.data.slice(0, 10),
    paginatedPosts.data.length > pageSize,
    page > 1,
    page
  ];
};
