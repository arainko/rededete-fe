import axios from "axios";
import { API, basicAuth } from "./common.js";

export const getPostComments = async (postId, maybeEmail, maybePassword) => {
  return await axios.get(`${API}/api/comment/${postId}`, basicAuth(maybeEmail, maybePassword));
};

export const deleteComment = async (postId, commentId, email, password) => {
  return await axios.delete(`${API}/api/comment/${postId}/${commentId}`, basicAuth(email, password));
};

export const createComment = async (postId, content, email, password) => {
  return await axios.post(`${API}/api/comment/${postId}`, {content}, basicAuth(email, password));
};
