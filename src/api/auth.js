import axios from "axios";
import { API, basicAuth } from "./common.js";

export const loginRequest = async (email, password) => {
  return await axios
    .get(`${API}/api/users/me`, basicAuth(email, password));
};

export const registerRequest = async (username, email, password, repeatedPassword) => {
  return await axios
    .post(`${API}/api/users/register`, { username, email, password, repeatedPassword });
};

export const updateUserPassword = async (email, password, newPassword, repeatedPassword) => {
  const data = {
    email,
    password: newPassword,
    repeatedPassword
  };
  return await axios
    .put(`${API}/api/users/me/password`, data, basicAuth(email, password));
};
