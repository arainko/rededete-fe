import axios from "axios";

export const API = process.env.VUE_APP_API_HOST;

export const basicAuth = (email, password) => {
  return {
    auth: {
      username: email,
      password: password
    }
  };
};
