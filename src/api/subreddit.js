import axios from "axios";
import {API, basicAuth} from "./common.js";

export const getAllSubreddits = async () => {
  return await axios.get(`${API}/api/subreddit`);
};

export const getSubreddit = async (name, email, password) => {
  return await axios.get(`${API}/api/subreddit/name/${name}`, basicAuth(email, password));
};

export const getSubscribedToSubs = async (email, password) => {
  return await axios.get(`${API}/api/subreddit/me`, basicAuth(email, password));
};

export const getSubsContaining = async query => {
  return await axios.get(`${API}/api/subreddit/search`, {
    params: {
      name: query
    }
  });
};

export const subscribe = async (subName, email, password) => {
  return await axios.post(`${API}/api/subreddit/subscribe/${subName}`,
    {},
    basicAuth(email, password)
  );
};

export const unsubscribe = async (subName, email, password) => {
  return await axios.post(`${API}/api/subreddit/unsubscribe/${subName}`,
    {},
    basicAuth(email, password)
  );
};

export const updateDesc = async (subName, description, email, password) => {
  return await axios.put(`${API}/api/subreddit/name/${subName}`, {description}, basicAuth(email, password));
};

export const createSubreddit = async (name, description, email, password) => {
  return await axios.post(
    `${API}/api/subreddit`, {
      name,
      description
    },
    basicAuth(email, password)
  );
};
