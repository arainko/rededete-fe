import axios from "axios";
import {API, basicAuth} from "./common.js";

export const uploadFile = async files => {
  const formData = new FormData();
  formData.append("image", files[0]);
  return await axios.post(`${API}/api/media/image`,formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
};

