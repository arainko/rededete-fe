import { createRouter, createWebHistory } from "vue-router";
import Login from "../views/Login.vue";
import SubredditPage from "../views/SubredditPage.vue";
import Profile from "../views/Profile.vue";
import Register from "../views/Register.vue";
import NotFoundPage from "../views/NotFoundPage.vue";
import CreatePost from "@/views/CreatePost";
import HomePage from "@/views/HomePage";
import PostPage from "@/views/PostPage";
import CreateSub from "@/views/CreateSub";
import SearchPage from "@/views/SearchPage";

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/search",
    name: "SearchPage",
    component: SearchPage
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  },
  {
    path: "/r/:subName",
    name: "SubredditPage",
    component: SubredditPage
  },
  {
    path: "/r/:subName/create",
    name: "CreatePost",
    component: CreatePost
  },
  {
    path: "/notfound",
    name: "NotFoundPage",
    component: NotFoundPage
  },
  {
    path: "/",
    name: "Home",
    component: HomePage
  },
  {
    path: "/r/:subName/:postId",
    name: "PostPage",
    component: PostPage
  },
  {
    path: "/createSub",
    name: "CreateSub",
    component: CreateSub
  }

];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
